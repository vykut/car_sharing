//
//  Journey.swift
//  CarSharing
//
//  Created by Victor Socaciu on 17/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation

protocol JourneyCodable {
    init?(dictionary: [String: Any])
}

struct Journey {
    var passengers: [User]
    var driver: Driver
    var driverArrivingTimes: [Date]
    
    var dictionary: [String:Any] {
        return [
            "passengers":passengers,
            "driver":driver,
            "driverArrivingTimes":driverArrivingTimes
        ]
    }
}

extension Journey: JourneyCodable {
    init?(dictionary: [String : Any]) {
        guard let passengers = dictionary["passengers"] as? [User],
        let driver = dictionary["driver"] as? Driver,
        let driverArrivingTimes = dictionary["driverArrivingTimes"] as? [Date] else { return nil }
        
        self.init(passengers: passengers, driver: driver, driverArrivingTimes: driverArrivingTimes)
    }
}
