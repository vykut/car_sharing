//
//  Driver.swift
//  CarSharing
//
//  Created by Victor Socaciu on 17/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation

protocol DriverCodable {
    init?(dictionary: [String:Any])
}

struct Driver {
    var user: User
    var carModel: String
    var carColor: String
    var numberPlate: String
    var leavingTime: Date
    var arrivalTime: Date
    var numberOfPassengers: Int
    
    var dictionary: [String:Any] {
        return [
            "user":user.dictionary,
            "carModel":carModel,
            "carColor":carColor,
            "numberPlate":numberPlate,
            "leavingTime":leavingTime,
            "arrivalTime":arrivalTime,
            "numberOfPassengers":numberOfPassengers
        ]
    }
}

extension Driver: DriverCodable {
    init?(dictionary: [String : Any]) {
        guard let user = dictionary["user"] as? User,
            let carModel = dictionary["carModel"] as? String,
            let carColor = dictionary["carColor"] as? String,
            let numberPlate = dictionary["numberPlate"] as? String,
            let leavingTime = dictionary["leavingTime"] as? Date,
            let arrivalTime = dictionary["arrivalTime"] as? Date,
            let numberOfPassengers = dictionary["numberOfPassengers"] as? Int else { return nil }
        
        self.init(user: user, carModel: carModel, carColor: carColor, numberPlate: numberPlate, leavingTime: leavingTime, arrivalTime: arrivalTime, numberOfPassengers: numberOfPassengers)
    }
}
