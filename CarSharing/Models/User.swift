//
//  User.swift
//  CarSharing
//
//  Created by Victor Socaciu on 17/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation
import MapKit

protocol UserCodable {
    init?(dictionary: [String:Any])
}

struct User {
    
    var devID: String
    var name: String
//    var surname: String
    var location: CLLocationCoordinate2D
    var phone: String
    var cohort: String
//    var timeOnMap: TimeInterval
    var marker: String
    var homeAddress: CLLocationCoordinate2D
    
    var dictionary: [String:Any] {
        return [
            "devID":devID,
            "name":name,
//            "surname":surname,
            "long":location.longitude,
            "lat":location.latitude,
            "phone":phone,
            "cohort":cohort,
//            "timeOnMap":timeOnMap,
            "marker":marker,
            "homeAddressLong":homeAddress.longitude,
            "homeAddressLat":homeAddress.latitude
        ]
    }
}

extension User: UserCodable {
    init?(dictionary: [String:Any]) {
        guard let devID = dictionary["devID"] as? String,
            let name = dictionary["name"] as? String,
//            let surname = dictionary["surname"] as? String,
            let long = dictionary["long"] as? Double,
            let lat = dictionary["lat"] as? Double,
            let phone = dictionary["phone"] as? String,
            let cohort = dictionary["cohort"] as? String,
//            let timeOnMap = dictionary["timeOnMap"] as? TimeInterval,
            let marker = dictionary["marker"] as? String,
            let homeAddressLong = dictionary["homeAddressLong"] as? Double,
            let homeAddressLat = dictionary["homeAddressLat"] as? Double else { return nil }
        
        self.init(devID: devID, name: name,/* surname: surname,*/ location: CLLocationCoordinate2D(latitude: lat, longitude: long), phone: phone, cohort: cohort,/* timeOnMap: timeOnMap,*/ marker: marker, homeAddress: CLLocationCoordinate2D(latitude: homeAddressLat, longitude: homeAddressLong))
    }
}
