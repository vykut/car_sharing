//
//  Emitter.swift
//  CarSharing
//
//  Created by Victor Socaciu on 22/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation
import UIKit

class Emitter {
    static func getEmitter() -> CAEmitterLayer {
        let emitter = CAEmitterLayer()
        
        emitter.emitterShape = .line
        emitter.emitterCells = getCells()
        
        return emitter
        
    }
    
    static func getCells() -> [CAEmitterCell] {
        var cells = [CAEmitterCell]()
        var images: [UIImage] = [UIImage(named: "conchiglie")!, UIImage(named: "farfalle")!, UIImage(named: "penne")!, UIImage(named: "macaroni")!, UIImage(named: "gnocchi")!]
        
        for i in 0..<5 {
            let cell = CAEmitterCell()
            cell.contents = images[i].cgImage
            cell.birthRate = 1 + Float(exactly: 0.7 * Float(i))!
            cell.beginTime = CFTimeInterval(exactly: 0.2 * Double(i))!
            cell.lifetime = 10
            cell.velocity = 100
            cell.emissionLongitude = .pi
            cell.spin = .pi / 2
            
            cell.scale = 0.09
            cell.scaleRange = 0.11
            
            cells.append(cell)
        }
        
        return cells
    }
}
