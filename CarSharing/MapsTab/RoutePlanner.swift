//
//  RoutePlanner.swift
//  CarSharing
//
//  Created by Victor Socaciu on 18/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation
import MapKit

protocol RoutePlannerDelegate: class {
    func sendRoute(route: (/*[User], */[MKRoute], TimeInterval))
}

class RoutePlanner: NSObject {
    
    var request = MKDirections.Request()
    
    weak var delegate: RoutePlannerDelegate?
    
    // MARK: - INPUT VARIABLES
    var passengersArray: [User] = [] {
        didSet {
            findRoute()
        }
    }
    
    var locationManager = CLLocationManager()
    
    var destination: CLLocationCoordinate2D
    
    // MARK: - OUTPUT VARIABLES
    var finalRoute: ([MKRoute], TimeInterval) = ([], 0) {
        // didSet is on the main thread
        didSet {
            if finalRoute.0.isEmpty { return }
            delegate?.sendRoute(route: finalRoute)
        }
    }
    
    // order of routes doesn't matter
    var routes: [MKRoute] = [] {
        didSet {
            if !routes.isEmpty, routes.count == passengersArray.count + 1 {
                let time = calculateTime(route: routes)
                finalRoute = (routes, time)
            }
        }
    }
    
    init(destination: CLLocationCoordinate2D) {
        
        request.transportType = .automobile
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        self.destination = destination
    }
    
    func findRoute() {
        routes.removeAll()
        
        if passengersArray.isEmpty {
            route(source: locationManager.location!.coordinate, destination: destination)
            return
        }
        
        // SEND USER INDEX TO ROUTE FUNCTION!!!
        
        // CHECK IF USER HAS GOOGLE MAPS DOWNLOADED WHEN SIGNING UP FOR THE APP
        
        // driver to first passenger
        if routes.isEmpty {
            route(source: locationManager.location!.coordinate, destination: passengersArray[0].location)
        }
        
        for i in 0..<passengersArray.count - 1 {
            route(source: passengersArray[i].location, destination: passengersArray[i + 1].location)
        }
            
        // last passenger to destination
        if routes.count != passengersArray.count + 1 {
            route(source: passengersArray.last!.location, destination: destination)
        }
    }
    
    func route(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        
        let sourcePlacemark = MKPlacemark(coordinate: source)
        let destinationPlacemark = MKPlacemark(coordinate: destination)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        request.source = sourceMapItem
        request.destination = destinationMapItem
        
        let directions = MKDirections(request: request)
        
        directions.calculate { (response, err) in
            
            // tell user about the error
            guard err == nil else { print(err!.localizedDescription); return }
            
            if self.passengersArray.isEmpty {
                self.finalRoute = (response!.routes, response!.routes.first!.expectedTravelTime)
            } else {
                self.routes.append(response!.routes.first!)
            }
        }
    }
    
    func calculateTime(route: [MKRoute]) -> TimeInterval {
        var time: TimeInterval = 0
        route.forEach { (mkRoute) in
            time += mkRoute.expectedTravelTime
        }
        return time
    }
}

//class RoutePlanner: NSObject {
//
//    private var request = MKDirections.Request()
//
//    private var locationManager = CLLocationManager()
//
//    var isFirstUpdate = true
//
//    weak var delegate: RoutePlannerDelegate?
//
//    // MARK: - INPUT VARIABLES
//    var passengersArray: [User] = [] {
//        didSet {
//            start()
//        }
//    }
//
//    var destination: CLLocationCoordinate2D = Constants.isAtAcademy ? user!.homeAddress : Constants.AcademyCoords // add didSet to start routing to the destination rigth away if it's driver
//
//    // MARK: - OUTPUT VARIABLES
//    var passengersPermutations: [[User]] = []
//
//    var possibleRoutes: [[MKRoute]] = []
//
//    var numberOfRequests: Int = 0 {
//        didSet {
//            if numberOfRequests == 0 {
//                findBestRoute()
//            }
//        }
//    }
//
//    var possibleDurations: [TimeInterval] = []
//
//    var bestRoute: (/*[User], */[MKRoute], TimeInterval) = (/*[], */[], 0) {
//        didSet {
//            delegate?.sendRoute(route: bestRoute)
//        }
//    }
//
//    // MARK: - INIT
//    override init() {
//        super.init()
//
//        request.requestsAlternateRoutes = true
//        request.transportType = .automobile
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.startUpdatingLocation()
//    }
//
//    func start() {
//        possibleRoutes = Array(repeating: [], count: 25)
//        if passengersArray.isEmpty {
//            generateRoute(for: [], index: 0)
//            return
//        }
//
//        passengersPermutations = permute(items: passengersArray)
//        //        passengersPermutations.forEach { (users) in
//        //            generateRoute(for: users)
//        //        }
//
//        for i in 0..<passengersPermutations.count {
//            generateRoute(for: passengersPermutations[i], index: i)
//        }
//    }
//
//    func findBestRoute() {
//        var timeArray: [TimeInterval] = []
//
//        for i in 0..<possibleRoutes.count {
//            let time = calculateTime(routes: possibleRoutes[i])
//            timeArray.append(time)
//        }
//
//        let minTime = timeArray.min() ?? 0
//        let index = timeArray.firstIndex(of: minTime) ?? 0
//
//        bestRoute = (/*passengersPermutations[index], */possibleRoutes[index], timeArray[index])
//    }
//
//    func generateRoute(for users: [User], index: Int) {
//
//        if users.isEmpty {
//            route(source: locationManager.location!.coordinate, destination: destination, index: index)
//            return
//        }
//
//        // generate route from driver to first passenger
//        route(source: locationManager.location!.coordinate, destination: users[0].location, index: index)
//
//        //generate route between each passenger
//        for i in 0..<users.count - 1 {
//            route(source: users[i].location, destination: users[i + 1].location, index: index)
//        }
//
//        //generate route from last passenger to the academy
//        route(source: users.last!.location, destination: destination, index: index)
//    }
//
//    func route(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, index: Int) {
//        var route = MKRoute()
//
//        print("Source coordinates: \(source)\n")
//        print("Destination coordinates: \(destination)\n")
//
//        let sourcePlacemark = MKPlacemark(coordinate: source)
//        let destinationPlacemark = MKPlacemark(coordinate: destination)
//
//        //        print(sourcePlacemark.coordinate)
//        //        print(destinationPlacemark.coordinate)
//
//        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
//        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
//
//        //        print(sourceMapItem.placemark.coordinate)
//        //        print(destinationMapItem.placemark.coordinate)
//
//        request.source = sourceMapItem
//        request.destination = destinationMapItem
//
//
//
//        let directions = MKDirections(request: request)
//        numberOfRequests += 1
//        directions.calculate { (response, err) in
//            guard err == nil else { print(err!.localizedDescription); return }
//            // show error to user
//            route = response!.routes.sorted(by: { (route1, route2) -> Bool in
//                return route1.expectedTravelTime < route2.expectedTravelTime
//            }).first!
//
//            // YOU LEFT HERE; YOU NEED TO MAKE THE FUNCTION ESCAPING!!!!!!!!
//            print("Route is: \(route.expectedTravelTime / 60) minutes")
//            self.possibleRoutes[index].append(route)
//            self.numberOfRequests -= 1
//        }
//    }
//
//
//    func permute(items: [User]) -> [[User]] {
//        var scratch = Array(items) // This is a scratch space for Heap's algorithm
//        var result: [[User]] = [] // This will accumulate our result
//
//        // Heap's algorithm
//        func heap(_ n: Int) {
//            if n == 0 {
//                return
//            }
//            if n == 1 {
//                result.append(scratch)
//                return
//            }
//
//            for i in 0..<n-1 {
//                heap(n-1)
//                let j = (n%2 == 1) ? 0 : i
//                scratch.swapAt(j, n-1)
//            }
//            heap(n-1)
//        }
//
//        // Let's get started
//        heap(scratch.count)
//
//        // And return the result we built up
//        print("Result from permute function: \(result)")
//        return result
//    }
//
//    func calculateTime(routes: [MKRoute]) -> TimeInterval {
//        var total = TimeInterval(0)
//        if routes.isEmpty {
//            return 999999
//        }
//        routes.forEach { (route) in
//            total += route.expectedTravelTime
//        }
//        print("Total journey time: \(total)")
//        return total
//    }
//}
