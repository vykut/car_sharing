//
//  PastaOverlay.swift
//  CarSharing
//
//  Created by Victor Socaciu on 19/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import UIKit
import MapKit

class PastaPolyline: PastaAnnotation, MKOverlay {
    var boundingMapRect: MKMapRect
}
