//
//  InfoViewController.swift
//  CarSharing
//
//  Created by Victor Socaciu on 22/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBAction func dismissMessage(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
