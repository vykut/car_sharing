//
//  MapsViewController.swift
//  CarSharing
//
//  Created by Victor Socaciu on 16/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import UIKit
import MapKit
import SwiftLocation
import AVKit
import FirebaseFirestore

class MapsViewController: UIViewController {
    
    // MARK: - OUTLETS
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var mapView: PastaMapView!
    @IBOutlet weak var etaContainer: RoundView!
    @IBOutlet weak var etaLabel: UILabel!
    @IBOutlet weak var saveStartCostButton: RoundButton!
    
    // MARK: - DRIVER OUTLETS
    @IBOutlet weak var driverDestinationContainer: RoundView!
    @IBOutlet weak var driverDestinationLabel: UILabel!
    @IBOutlet weak var backButton: RoundButton!
    
    // MARK: - PASSENGER OUTLETS
    @IBOutlet weak var passengerWaitContainer: UIView!
    @IBOutlet weak var passengerWaitFindingDriver: UILabel!
    @IBOutlet weak var passengerPickupContainer: RoundView!
    @IBOutlet weak var passengerPickupLabel: UILabel!
    @IBOutlet weak var passengerPastaAnimationContainer: UIView!
    
    // MARK: - VARS
    
    // or use only the location of the users and save the users in constants (but the array is not constant lol)
    // this is just to have an
    var passengersArray: [User] = [] {
        didSet {
            mapView.removeOverlays(mapView.overlays)
            routePlanner.passengersArray = passengersArray
            if passengersArray.count != 0 {
                setSaveLabel()
            } else {
                setGoLabel()
            }
        }
    }
    
    var destination: CLLocationCoordinate2D = Constants.isAtAcademy ? user!.homeAddress : Constants.AcademyCoords {
        didSet {
            routePlanner.destination = destination
        }
    }
    
    var route: [MKRoute] = []
    
    // don't add the drivers to the passengers, from firebase
    let passengers: [User] = [
        User(devID: "deviceID1", name: "name1",/* surname: surname1,*/ location: CLLocationCoordinate2D(latitude: 40.842675, longitude: 14.206756), phone: "phone1", cohort: "Morning",/* timeOnMap: 120,*/ marker: "conchiglieMarker", homeAddress: CLLocationCoordinate2D(latitude: 40.842675, longitude: 14.206756)),
        User(devID: "deviceID2", name: "name2",/* surname: surname2,*/ location: CLLocationCoordinate2D(latitude: 40.844454, longitude: 14.239083), phone: "phone2", cohort: "Morning",/* timeOnMap: 120,*/ marker: "farfalleMarker", homeAddress: CLLocationCoordinate2D(latitude: 40.844454, longitude: 14.239083)),
        User(devID: "deviceID3", name: "name3",/* surname: surname3,*/ location: CLLocationCoordinate2D(latitude: 40.855456, longitude: 14.250697), phone: "phone3", cohort: "Morning",/* timeOnMap: 120,*/ marker: "gnocchiMarker", homeAddress: CLLocationCoordinate2D(latitude: 40.855456, longitude: 14.250697)),
        User(devID: "deviceID4", name: "name4",/* surname: surname4,*/ location: CLLocationCoordinate2D(latitude: 40.860006, longitude: 14.273482), phone: "phone4", cohort: "Morning",/* timeOnMap: 120,*/ marker: "macaroniMarker", homeAddress: CLLocationCoordinate2D(latitude: 40.860006, longitude: 14.273482))
    ]
    
    var locationManager = CLLocationManager()
    
    var routePlanner: RoutePlanner!
    
    // delete all other markers after saving
    var isSaved = false
    
    var isFirstUserLocationUpdate = false
    
    var isNearADA = false
    
    var moviePlayer = AVPlayerViewController()
    
    // MARK: - FUNCTIONS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        routePlanner = RoutePlanner(destination: destination)
        routePlanner.delegate = self
        
        // retrieve data from server
        
        passengers.forEach { (user) in
            let annotation = PastaAnnotation()
            annotation.coordinate = user.location
            annotation.user = user
            mapView.addAnnotation(annotation)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // should be moved to login page
        Locator.requestAuthorizationIfNeeded()
        
        if !Constants.isDriver {
            startPastaAnimation()
        }
    }
    
    
    
    func startPastaAnimation() {
        
        let emitter = Emitter.getEmitter()
        emitter.emitterPosition = CGPoint(x: view.center.x, y: 0)
        emitter.emitterSize = CGSize(width: view.frame.width, height: 2)
        emitter.renderMode = .additive
        //        emitter.seed = 23
        //        emitter.spin = 50
        passengerPastaAnimationContainer.layer.addSublayer(emitter)
    }
    
    func setupView() {
        if Constants.isDriver {
            mapContainer.isHidden = false
            // if driver is in proximity of ADA (200 meters) show destinations container
            
            // otherwise setup by default route to ADA
        } else {
            // if route is already planned show the map with the route
            mapContainer.isHidden = true
            passengerWaitContainer.isHidden = false
        }
        
    }
    
    // when driver's position is in proximity to ADA
    @IBAction func driverDestinationList(_ sender: Any) {
        // make animation to expand the color of the button on the whole screen where is a view controller container with search options (maybe history locations too)
        
    }
    
    @IBAction func saveStartCost(_ sender: UIButton) {
        if Constants.isDriver {
            
            if passengersArray.isEmpty {
                setGoState()
                return
            }
            
            if isSaved {
                setGoState()
            } else {
                setSaveState()
                isSaved = true
            }
            
        } else {
            // make modal view to see the cost algorithm
            // example: https://youtu.be/k-GvIqh5Xcs
            // cost algorithm should use accelerometer for distance
        }
    }
    
    func checkIfNearADA() {
        
    }
    
    func setSaveState() {
        backButton.isHidden = false
        setGoLabel()
        // to be added functionality regarding notifications and server communication
        // in short: notify users that a driver is assigned to take them and send the details to firebase
    }
    
    func setGoState() {
        // what the hell do i do here?
        // notify passengers driver left
        startRoute()
    }
    
    func setSaveLabel() {
        UIView.animate(withDuration: 0.2) {
            self.saveStartCostButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 17)
            self.saveStartCostButton.setTitle(Constants.MapTab.saveRoute, for: .normal)
        }
    }
    
    func setGoLabel() {
        UIView.animate(withDuration: 0.2, animations: {
            self.saveStartCostButton.setTitle(Constants.MapTab.go, for: .normal)
        }) { isCompleted in
            self.saveStartCostButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 34)
        }
    }
    
    func startRoute() {
        // send user to google maps to start route
        var urlString = "https://www.google.com/maps/dir/?api=1"
        
        urlString += "&origin=\(mapView.userLocation.coordinate.latitude),\(mapView.userLocation.coordinate.longitude)"
        
        if !passengersArray.isEmpty {
            urlString += "&waypoints="
            passengersArray.forEach { (user) in
                urlString += "\(user.location.latitude),\(user.location.longitude)|"
            }
            urlString.removeLast(1)
        }
        
        urlString += "&destination=\(destination.latitude),\(destination.longitude)&dir_action=navigate&travelmode=driving"
        
        // model of url:
        // https://www.google.com/maps/dir/?api=1&origin=40.83752087748362,14.310160280380451&destination=40.836756,14.305946&dir_action=navigate&travelmode=driving
        
        print(urlString)
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        let url = URL(string: encodedUrlString) ?? URL(string: "www.google.com")!
        
        print(url)
        
        UIApplication.shared.open(url, options: [:]) { (routeStarted) in
            if routeStarted {
                // notify passengers that driver started the journey
            } else {
                // maybe add banner
                print("The route couldn't be started")
            }
        }
    }
    
    func selectSaveMarker() {
        guard let annotation = mapView.selectedAnnotations.first! as? PastaAnnotation else { return }
        mapView.deselectAnnotation(annotation, animated: false)
        if passengersArray.contains(where: { (user) -> Bool in
            user.devID == annotation.user.devID
        }) {
            passengersArray.removeAll { (user) -> Bool in
                user.devID == annotation.user.devID
            }
            return
        }
        
        // maybe add banner pod for errors / messages like these
        guard passengersArray.count < Constants.numberOfPassengers else { return }
        passengersArray.append(annotation.user)
    }
    
    func selectGoMarker() {
        // maybe delete all markers and show only the ones in the route and set the callout variable to true
    }
    
    @IBAction func zoomToUser(_ sender: Any) {
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        
        isSaved = false
        
        backButton.isHidden = true
        
        if passengersArray.isEmpty {
            setGoLabel()
        } else {
            setSaveLabel()
        }
    }
    
    func checkUserLocation() {
        let location = CLLocation(latitude: Constants.AcademyCoords.latitude, longitude: Constants.AcademyCoords.longitude)
        let distanceToADA = location.distance(from: mapView.userLocation.location!)
        if  distanceToADA < 200 {
            print(distanceToADA)
            destination = user!.homeAddress
            driverDestinationLabel.isHidden = false
            Locator.location(fromCoordinates: mapView.userLocation.coordinate, locale: nil, using: nil, timeout: nil, onSuccess: { (places) -> (Void) in
                self.driverDestinationLabel.text = places.first!.thoroughfare
                self.routePlanner.findRoute()
            }, onFail: { (err) -> (Void) in
                self.routePlanner.findRoute()
                return
            })
        } else {
            routePlanner.findRoute()
        }
    }
    
    func updateDB() {
        let db = Firestore.firestore()
        db.collection("Users").document("Victor").updateData(["long":mapView.userLocation.coordinate.longitude, "lat":mapView.userLocation.coordinate.latitude])
    }
}

extension MapsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !isFirstUserLocationUpdate {
            let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            mapView.setRegion(region, animated: true)
            isFirstUserLocationUpdate = true
            // check if user is in vecinity of ADA
            
            // updateDB()
            checkUserLocation()
            
        }
        if Constants.isDriver {
            updateDB()
        }
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let annotation = annotation as? MKUserLocation {
            annotation.title = ""
            return nil
        }
        
        guard let annotation = annotation as? PastaAnnotation else { return nil }
        
        var marker = mapView.dequeueReusableAnnotationView(withIdentifier: Constants.MapTab.marker)
        
        if marker == nil {
            marker = MKAnnotationView(annotation: annotation, reuseIdentifier: Constants.MapTab.marker)
            marker?.canShowCallout = false
        } else {
            marker?.annotation = annotation
        }
        
        marker?.image = UIImage.init(named: annotation.user.marker)
        //        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        //        button.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        //        button.tintColor = .carBlue
        
        //        marker?.rightCalloutAccessoryView = button
        return marker
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // isSaved == false => starts routing through the point/s
        // isSaved == true => can see name & call button
        // make array to store the selected annotations
        
        if Constants.isDriver {
            if !isSaved {
                selectSaveMarker()
            } else {
                selectGoMarker()
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = .carBlue
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        return MKOverlayRenderer()
    }
}

extension MapsViewController: RoutePlannerDelegate {
    func sendRoute(route: (/*[User], */[MKRoute], TimeInterval)) {
        // start drawing lines on the map
        route.0.forEach { (segment) in
            self.mapView.addOverlay(segment.polyline)
            self.mapView.setVisibleMapRect(segment.polyline.boundingMapRect, animated: true)
        }
        
        self.route = route.0
        
        // update label
        etaLabel.text = "\(Int(route.1 / 60)) mins"
    }
}
