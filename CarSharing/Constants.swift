//
//  Constants.swift
//  CarSharing
//
//  Created by Victor Socaciu on 16/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation
import MapKit

// reference to the current user of the app which may be either a passenger or a driver
internal var user: User? = nil
internal var driver: Driver?
// internal var passengers: [User?] = nil

struct Constants {
    
    static let afternoon = "Afternoon"
    static let morning = "Morning"
    
    static var numberOfPassengers = 4
    
    static var isDriver = true
    static var isAtAcademy = false
    static let isDriverDescription = "isDriver"
    
    static let AcademyCoords = CLLocationCoordinate2D(latitude: 40.836756, longitude: 14.305946)
//    static let AcademyCoords = CLLocationCoordinate2D(latitude: 40.842675, longitude: 14.206756)
    
    struct MapTab {
        static let saveRoute = "Save route and\nnotify passengers"
        static let go = "GO"
        static let cost = "Cost"
        static let pickup = "Pick-up time "
        static let mins = "mins"
        static let marker = "PastaMarker"
    }
    
    struct Marker {
        static private let marker = "Marker"
        static let farfalle = "farfalle" + marker
        static let gnocchi = "gnocchi" + marker
        static let conchiglie = "conchiglie" + marker
        static let macaroni = "macaroni" + marker
        static let penne = "penne" + marker
    }
}
