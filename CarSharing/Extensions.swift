//
//  Extensions.swift
//  CarSharing
//
//  Created by Victor Socaciu on 16/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    /// Grapefruit
    // hexa - #FF5A5A
    static let carRed: UIColor = UIColor(red: 1.0000000000, green: 0.3529411765, blue: 0.3529411765, alpha: 1.00)
    
    /// Pale Orange
    // hexa - #FFA55A
    static let carOrange: UIColor = UIColor(red: 1.0000000000, green: 0.6470588235, blue: 0.3529411765, alpha: 1.00)
    
    /// Sea
    // hexa - #369999
    static let carBlue: UIColor = UIColor(red: 0.2117647059, green: 0.6000000000, blue: 0.6000000000, alpha: 1.00)
    
    /// Leafy Green
    // hexa - #48CC48
    static let carGreen: UIColor = UIColor(red: 0.2823529412, green: 0.8000000000, blue: 0.2823529412, alpha: 1.00)
}
