//
//  RoundView.swift
//  CarSharing
//
//  Created by Victor Socaciu on 16/10/2018.
//  Copyright © 2018 The Romans. All rights reserved.
//

import UIKit

class RoundView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
}
